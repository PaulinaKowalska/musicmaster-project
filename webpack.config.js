module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        path: __dirname + '/public',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',

                // Exclude /node_modules directory
                exclude: /node_modules/,

                // Only run `.js` and `.jsx` files through Babel
                test: /\.js$/,
                query: {
                    // plugins: ['transform-runtime'],
                    presets: ['es2015', 'react']
                }
            },

            {
                test: /\.css$/, loader: "style-loader!css-loader"
            },
        ]
    },
    devServer: {
        port: 3000,
        contentBase: './public',
        inline: true
    },

}
